prj = lazy
all: run

run: $(prj)
	./$^
clean:
	rm -rf *.pyc *.o